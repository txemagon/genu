# ECS
```mermaid
classDiagram

    
    ComponentArrayVirtual --|> ComponentArray

class Coordinator {
    +init() void
    +createEntity() Entity
    +DestroyEntity(const Entity & entity) void
    +registerComponent() void
    +addComponent(const Entity & entity, const T & component) void
    +removeComponent(const Entity & entity) void
    +getComponent(const Entity & entity) T
    +getComponentType() ComponentType
    +registerSystem() shared_ptr~T~
    +setSystemSignature(const Signature & signature) void

    -MComponent componentManager
    -MEntity entityManager
    -MSystem systemManager

}


class ComponentArray  {
    +insertData(const Entity & entity, const T & component) void
    +removeData(const Entity & entity) void
    +getData(const Entity & entity) T
    +entityDestroyed(const Entity & entity) void

    -componentArray;
    -Entity entityToIndexMap
    -Entity indexToEntityMap
    -size_t size
}

class ComponentArrayVirtual {
    <<interface>>
    virtual  ~ComponentArrayVirtual()

    virtual entityDestroyed(const Entity & entity)

}

class ComponentType {
<<type>> 
uint8_t
ComponentType MAX_COMPONENTS;
}


class Entity {
<<type>> 
uint16_t
ComponentType MAX_ENTITIES;
}

class Signature {
<<type>> 
bitset~MAX_COMPONENTS~
}
```

# Managers
```mermaid
classDiagram
    class MComponent {
        +registerComponent() void
        +getComponentType() ComponentType
        +addComponent(Entity entity, T component) void
        +removeComponent(Entity entity) void
        +getComponent(Entity entity) T
        +entityDestroyed(Entity entity) void

        -componentTypes unordered_map~const char*, ComponentType~ 
        -componentArrays unordered_map~const char*, shared_ptr~ComponentArrayVirtual~~;
        -nextComponentType ComponentType
        -getComponentArray() shared_ptr~ComponentArray~T~~
    }


    class MEntity {
        +MEntity() 
        +createEntity() Entity
        +destroyEntity(Entity entity) void
        +setSignature(Entity entity, Signature signature) void
        +getSignature(Entity entity) Signature

        -unusedEntitiesIDs queue~Entity~
        -signatures array~Signature, MAX_ENTITIES~
        -usedEntitiesIDs Entity
    }

    class  MSystem{
        +registerSystem() shared_ptr~T~
        +setSignature(Signature signature) void
        +entityDestroyed(Entity entity) void
        +entitySignatureChanged(Entity entity, Signature entitySignature) void

        -signatures unordered_map~const char*, Signature~
        -systems unordered_map~const char*, std::shared_ptr~System~~
    }
```



# Dynamic & Kinematic
```mermaid
    classDiagram
        

        class CDynamic {
        <<struct>>
        vfree* resultant;
        vfree* torque;
        vfixed* components;
    }

        class SDynamic {
    
        +init() void

        +update() void

        +Momentum(const Entity & entity) double 
    }

    class CKinematic{
    <<struct>>
        vfree* position;
        vfree* velocity;
        vfree* acceleration;
    }

    class CKListTrail{
    <<struct>>
        vector<CKTrail> trail
    }

    class CKTrail{
    <<struct>>
        vfree acceleration;
        vfree position;
        vfree velocity;
        float time;
    }


    class SKinematic {

        +void init()
        +void update()

        -vector<vfree> acceleration;
        -vector<vfree> position;
        -vector<vfree> velocity;
    }

    class STrail {

        +init() void

        +update(Entity entity) void

        +getDifferential(Entity entity, int firstMoment = -1, int lastMoment = -1) vfree

    }

    System --|> SDynamic
    System --|> SKinematic
    System --|> STrail
    
    class System{
        +set~Entity~ entities;
    }
```

