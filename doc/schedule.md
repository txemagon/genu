```mermaid
gantt
    title  GENU Schedule
    dateFormat  YY-MM-DD
    section Design
    Read project instructions: 20-12-05, 1d
    The lay of the land (MA): 20-12-05, 2d
    Complete presentation (VT y AC): 20-12-05 ,2d
    See RUBE tutorials: 20-12-07, 1d
    Design WireFrame: 20-12-08, 4d

    section Presentation
    Slide writing: 21-05-15, 21d
    Resource planning: 21-05-25, 6d
    Print & Bind: 21-06-01, 6d
    Presentation: 21-06-07, 1d 
```