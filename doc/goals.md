# Goals

- [x] Todos: Ver el [vídeo del profesor RetroMan sobre ECS](https://www.youtube.com/watch?v=TaV6L3VYiaY)
- [x] VC: Subir el pdf.
- [ ] 3A: Definir el diagrama de GANTT
- [ ] ~~3A: Esbozar la historia del juego~~
- [ ] ~~MA: Conseguir un diseñador~~
- [x] Txema: Subir la estructura de la memoria.
- [ ] Txema: Esbozar el diagrama de clases.
- [x] 3A: Ver el programa [RUBE Box2d](https://www.iforce2d.net/rube/)
- [ ] Todos: Ver y tomar apuntes de la lista de tutoriales de [RUBE](https://www.youtube.com/playlist?list=PLzidsatoEzejt4smjfpeOd-XDjtE6PFr9).
- [x] Crear una lista con enlaces a juegos o programas en la que nos podamos inspirar para definir el objetivo. Dentro del directorio doc/research.
- [ ] 3A: Leer todos los comentarios de doc/proyecto
- [x] MA: Completar la lista de videojuegos que han sido game changer. Asegurarse de que tienen mínimo 1 imagen, un link a los videos, el año de publicación, compañía, plataforma y género.
- [ ] VT y AC: completar presentacion