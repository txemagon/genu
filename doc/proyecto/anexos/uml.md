```mermaid
classDiagram

    %%%%%%%%%%%%%%%%%%%%%%%%
    %%%%    Dependency  %%%%
    %%%%%%%%%%%%%%%%%%%%%%%%

    System --> Component
    Entity --> System
    PositionSystem --> VectorComponent
    WorldEntity --> PositionSystem


    %%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%    Composition  %%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%  

    Entity *-- Component

    %%%%%%%%%%%%%%%%%%%%%%%%
    %%%%    Inheritance %%%%
    %%%%%%%%%%%%%%%%%%%%%%%%

    System <|--  PositionSystem
    System <|--  LayerSystem

    Component <|--  VectorComponent

    Entity <|-- WorldEntity
    Entity <|-- GeometryEntity

    GeometryEntity <|-- ShapeEntity
    GeometryEntity <|-- PointEntity
    GeometryEntity <|-- VectorEntity

    ShapeEntity <|-- CompoundShapeEntity
    ShapeEntity <|-- TriangleEntity
    ShapeEntity <|--  SegmentEntity
    
    Event <|-- ComponentsEvent
    Event <|-- WorldChangeEvent

    %%%%%%%%%%%%%%%%%%
    %%%%  SYSTEMS %%%%
    %%%%%%%%%%%%%%%%%%

    class System {
    <<abstract>> 
    *int uses
    }

    class LayerSystem {
    +hide()
    +move()
    +displace()
    +unassign()
    }

    class PositionSystem {
    +assign()
    +move()
    +displace()
    +unassign()
    +sum(PointEntity, VectorEntity) PointEntity
    +sub(PointEntity, PointEntity) VectorEntity
    }

    %%%%%%%%%%%%%%%%%%%%%%
    %%%%  COMPONENTS  %%%%
    %%%%%%%%%%%%%%%%%%%%%%
    

    class Component {
    <<abstract>> 
    +init()
    +destroy()
    }

    class VectorComponent {
    <<struct>>
    +vec3 Position
    }

    %%%%%%%%%%%%%%%%%%
    %%%%  ENTITYS %%%%
    %%%%%%%%%%%%%%%%%%

    class Entity {
    <<abstract>> 
    +init()
    +destroy()
    }

    class WorldEntity { }

    class GeometryEntity { <<abstract>> }

    class PointEntity { 
    +getPosition() VectorComponent
    +setPosition(VectorComponent)
    -VectorComponent position 
    }

    class VectorEntity {
    +getVector() VectorComponent
    +setVector(VectorComponent)
    +VectorComponent vector
    }

    class ShapeEntity { <<abstract>> }

    class SegmentEntity { }

    class TriangleEntity { }

    class CompoundShapeEntity { }

    %%%%%%%%%%%%%%%%%%%%%%%%
    %%%%    HANDLERS    %%%%
    %%%%%%%%%%%%%%%%%%%%%%%%

    class ComponentMemoryHandler { }

    class EventHandler { }

    %%%%%%%%%%%%%%%%%%%%
    %%%%    EVENTS  %%%%
    %%%%%%%%%%%%%%%%%%%%

    class Event { <<abstract>> }

    class ComponentsEvent { }

    class WorldChangeEvent{ }

    %%%%%%%%%%%%%%%%%%%%%%%%
    %%%%    MATHLIBRARY %%%%
    %%%%%%%%%%%%%%%%%%%%%%%%

    class vec3 { 
    vec3(int x, int y ) vec3
    vec3(int x, int y, int z ) vec3
    -float x
    -float y
    -float z
    -float module
    -float alpha
    -float beta
    -float determinant
    -float unitVector

    +getX() float
    +getY() float
    +getZ() float
    +getModule() float
    +getAlpha() float
    +getBeta() float
    +getDeterminant() float
    +getUnitVector() float
    +operator+(vec3) 
    +opertator+=(vec3) 
    +operator-(vec3) 
    +operator-=(vec3) 
    +operator*(vec3) 
    +operator*=(vec3) 

    }

```
