# Referencias 

- Samurai Showdown
- Stranger Things 3 The Game

- The Walking Dead Telltale Games
- Nuclear Thorne
- The Binding of Isaac
- Dead Cells

## Juegos Isometricos
- Una de las dificultades de estos es encontrar Spritesheets.
- Diablo

### Amstrad CPC
- Binky: Juego simple sin cambio de plano los enemigos siguen algoritmo a *.
- Gyroscope : Incluye planos inclinados.
- Bobby Bearing : Planos inclinados y enemigos con ia simple.
- Gauntlet 
- The Great Escape: Aventura grafica
- Batman
- Head Over Heels

### Juegos DOS
- Shogun Total War: Incluye generador de terreno ia individual y colectiva.
- Starcraft 1 
- Warcraft
- LEISURE SUIT LARRY 6
- The Secret of Monkey Island
- Indiana Jones and the Fate Of Atlantis
- Little Big Adventure Longplay

### Windows

- Wolfenstein 3D
- Doom 3D
- Half-Life 1998

### Plataforma Web
- Habbo

## Juegos de Lucha

# Tipos de Motores de Video Juegos

- Basados en Baldosas: Se especifican las imágenes que aparecen en el mapa en cada hueco del mismo.
  - Rpg Maker. 
- De Lucha
  - M.U.G.E.N: Open Source
- De Fisicas
  - Box 2D.(R.U.B.E)
  - CUDA(Nvidia)
- De Render
  - Ogre
- De Audio: No hay motores como tal, Existen API's
  - API de sonido de DirectX.
  - XAudio2 API de audio de nivel inferior para Windows.
  - [audio lib bass (dynamic) Windows/Linux 32/64-bit](https://www.freebasic.net/forum/viewtopic.php?t=23394).
- De 3D
- De 

# Analisis/Conclusiones

- Ideas iniciales:
  - Desarrollar un videojuego.
  - Desarrollar un motor de físicas para Solidos/Rigidos

## Objetivo Final: Motor de físicas para Solidos/Rigidos
- Motivos a favor
  - Es una idea que nos apetece a todos los integrantes
  - Nos permitira aumentar nuestros conocimientos en: Algoritmia/Matemáticos.
  - Nos permitira aumentar nuestros conocimientos en:
  - No necesitamos aprender modelado ni diseño 3D, unicamente incrementaríamos nuestros conocimientos en programación.  
- Motivos en Contra
  - Los gráficos se deberan realizar con un modelador 3D, para sacar con 8 cámaras los sprites de cada acción.
  - Falta de experiencia y personal de diseño y animacion.


